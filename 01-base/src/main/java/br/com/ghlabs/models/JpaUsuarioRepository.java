package br.com.ghlabs.models;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Primary
@Repository
public class JpaUsuarioRepository implements UsuarioRepository {

	@PersistenceContext(name="twinstterPU")
	private EntityManager entityManager;	
	
	public List<Usuario> obterTodos() {		
		String jql = "SELECT u FROM Usuario u";
		Query q;
		q = entityManager.createQuery(jql, Usuario.class);
		return q.getResultList();
	}
	
	//Retorna Usuários com o mesmo nome na busca
	public List<Usuario> resultadosBusca(String userName){
		String jql = "Select u from Usuario u WHERE u.nomeUsuario LIKE :userName";
		List<Usuario> usuarios = new ArrayList<Usuario>();
		try{
			usuarios = entityManager.createQuery(jql, Usuario.class).setParameter("userName", userName).getResultList();
		} catch(NoResultException e){
			return null;
		}
		return usuarios;
	}
	
	public void inserir(Usuario usuario) {		
		entityManager.persist(usuario);		
	}

}
