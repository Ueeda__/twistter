package br.com.ghlabs.models;

import java.util.List;

public interface UsuarioRepository {

	List<Usuario> obterTodos();
	
	void inserir(Usuario usuario);
	
	List<Usuario> resultadosBusca(String userName);
}
