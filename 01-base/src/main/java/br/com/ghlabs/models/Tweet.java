package br.com.ghlabs.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Tweet {

	@Id 
	@GeneratedValue
	private int idTweet;
	
	private Date publicacaoTweet;
	private String conteudoTweet;
	
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario donoTweet;
	
}
