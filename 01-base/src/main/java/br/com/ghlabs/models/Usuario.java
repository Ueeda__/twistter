package br.com.ghlabs.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;


@Entity 
public class Usuario {

	@Id
	@GeneratedValue
	private int idUsuario;
	
	@NotNull(message="Nome não pode estar vazio.")
	@Size(min=6)
	private String nomeUsuario;
	
	@NotEmpty
	@Pattern(regexp = ".+@.+\\.[a-z]+")
	private String emailUsuario;
	
	@NotNull(message="Senha não pode ser menor do que 6 caracteres.")
	@Size(min=6)
	private String senhaUsuario;
	
	private String imagemPerfil;
	
	@NotNull(message="Data de nascimento não pode ser vazia.")
	private String dataNascimento;
	
	@OneToMany(mappedBy="donoTweet")
	private List<Tweet> tweetsPublicados;
	
	@ManyToMany
	@JoinTable(name="usuario_has_retweet", joinColumns=
  	{@JoinColumn(name="idUsuario")}, inverseJoinColumns=
    {@JoinColumn(name="idTweet")})
	private List<Tweet> tweetsRetweetados;
	
	@ManyToMany
	@JoinTable(name="usuario_has_favorite", joinColumns=
  	{@JoinColumn(name="idUsuario")}, inverseJoinColumns=
    {@JoinColumn(name="idTweet")})
	private List<Tweet> tweetsFavoritos;
	
	@ManyToMany
	@JoinTable(name="usuario_has_followers", joinColumns=
  	{@JoinColumn(name="idUsuario")}, inverseJoinColumns=
    {@JoinColumn(name="id_usuario_follower")})
	private List<Usuario> followers;
	
	@ManyToMany
	@JoinTable(name="usuario_has_following", joinColumns=
  	{@JoinColumn(name="idUsuario")}, inverseJoinColumns=
    {@JoinColumn(name="id_usuario_following")})
	private List<Usuario> following;
	
	public Usuario(){
		imagemPerfil = null;
		tweetsFavoritos = new ArrayList<Tweet>();
		tweetsPublicados = new ArrayList<Tweet>();
		tweetsRetweetados = new ArrayList<Tweet>();
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getEmailUsuario() {
		return emailUsuario;
	}

	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}

	public String getSenhaUsuario() {
		return senhaUsuario;
	}

	public void setSenhaUsuario(String senhaUsuario) {
		this.senhaUsuario = senhaUsuario;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	
	
}
