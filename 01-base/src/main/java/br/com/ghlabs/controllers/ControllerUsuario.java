package br.com.ghlabs.controllers;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.models.UsuarioRepository;

@Controller
public class ControllerUsuario {
	
	@Autowired
	private UsuarioRepository repo;
	
	@RequestMapping(value="/cadastro", method= RequestMethod.GET)
	public String novoUsuario(Model model){
		model.addAttribute("usuario", new Usuario());
		return "cadastro-usuario";
	}
	
	@Transactional
	@RequestMapping(value="/cadastro", method=RequestMethod.POST )
	public String novo(@Valid Usuario usuario, BindingResult result, Model model) {		
		if(result.hasFieldErrors()) {			
			model.addAttribute("usuario", usuario);
			return "cadastro-usuario";			
		}	
		repo.inserir(usuario);
		return "redirect:/ola-mundo";
	}
	
	
}
