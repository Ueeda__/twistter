package br.com.ghlabs.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ghlabs.models.UsuarioRepository;

@Controller
public class AuthController {
	
	@Autowired
	private UsuarioRepository repo;
	
	@RequestMapping(value="/login", method= RequestMethod.GET)
	public String login(Model model, Authentication auth){
		if(auth != null)
			return "redirect:/olamundo";
		return "/login";
	}
}
