<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/header.jsp">
    <jsp:param name="pageTitle" value="Twinstter - Login"/>
</jsp:include>

<sf:form servletRelativeAction="/login">
	<label for="login">E-mail:</label>
	<input type="text" name="username" class="form-control input-sm" />
	
	<label for="password">Senha:</label>
	<input type="password" name="password" class="form-control input-sm" />

	<input type="submit" value="Entrar" class="btn btn-primary btn-md btn-block" />  
</sf:form>

<jsp:include page="/WEB-INF/views/footer.jsp" />