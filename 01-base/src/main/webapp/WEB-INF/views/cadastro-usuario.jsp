<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="/WEB-INF/views/header.jsp">
    <jsp:param name="pageTitle" value="Twinstter - Cadastro"/>
</jsp:include>

<div class="col-md-12 header-index">
      <div class="container">
          <div class="col-md-6 col-md-offset-6">
              <div class="row">
                  <div class="col-md-2 col-sm-offset-8 btn-entrar"><a href="/01-base/login">Login</a></div>
              </div>
          </div>
      </div>
  </div>
<div class="container">
     <div class="col-md-6 col-md-offset-3" style="height: 550px;">
         <div class="col-md-12 centro">
             <div>
                <h2 class="title-index"> Participe hoje do Twistter.</h2>
                <sf:form modelAttribute="usuario">
                	<div class="row linhaCadastro">
                		<div class="col-sm-4 label-cadastro">
	                		<sf:label path="nomeUsuario"> Nome: </sf:label>
	                	</div>
						<div class="col-sm-8">
							<sf:input path="nomeUsuario"/>
							<sf:errors path="nomeUsuario"> </sf:errors>
						</div>
                	</div>
                	
                	<div class="row linhaCadastro">
                		<div class="col-sm-4 label-cadastro">
	                		<sf:label path="emailUsuario"> E-mail: </sf:label>
	                	</div>
						<div class="col-sm-8">
							<sf:input path="emailUsuario"/>
							<sf:errors path="emailUsuario"> </sf:errors>
						</div>
                	</div>
                	
                	<div class="row linhaCadastro">
                		<div class="col-sm-4 label-cadastro">
	                		<sf:label path="dataNascimento"> Data de Nascimento: </sf:label>
	                	</div>
						<div class="col-sm-8">
							<sf:input path="dataNascimento"/>
							<sf:errors path="dataNascimento"> </sf:errors>
						</div>
                	</div>
                	
					<div class="row linhaCadastro">
                		<div class="col-sm-4 label-cadastro">
	                		<sf:label path="senhaUsuario"> Senha: </sf:label>
	                	</div>
						<div class="col-sm-8">
							<sf:input path="senhaUsuario"/>
							<sf:errors path="senhaUsuario"> </sf:errors>
						</div>
                	</div>			
					
	               	<input type="submit" class="btn-inscreva-se" value="Criar" />
                </sf:form>
                 <div class="col-md-12">
                   <p class="texto-index">Ao inscrever-se, voc� concorda com os Termos de Servi�o e a Pol�tica de Privacidade, incluindo o Uso de Cookies. Outros poder�o encontr�-lo por e-mail ou n�mero de celular, quando fornecido.</p>
                 </div>
             </div>
         </div>
     </div>
 </div>
 
<jsp:include page="/WEB-INF/views/footer.jsp" />
