<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Novo Produto</title>
</head>
<body>

<sf:form modelAttribute="produto">

<sf:label path="nome">Nome:</sf:label>
<sf:input path="nome"/>
<sf:errors path="nome"></sf:errors>

<sf:label path="preco">Pre�o:</sf:label>
<sf:input path="preco"/>
<sf:errors path="preco"></sf:errors>


<input type="submit" value="Salvar" />
</sf:form>

</body>
</html>